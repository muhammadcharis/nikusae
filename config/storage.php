<?php

return [
    'menuRestaurant'    => storage_path() . '/app/menuRestaurant',
    'user'              => storage_path() . '/app/user',
];
