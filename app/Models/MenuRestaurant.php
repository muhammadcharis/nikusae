<?php namespace App\Models;

use Config;
use App\Uuids;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class MenuRestaurant extends Model
{
    use HasFactory,Notifiable,Uuids;

    public $timestamps      = false;
    public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $fillable = [
        'name',
        'description',
        'image',
        'price',
        'created_user_id',
        'updated_user_id',
        'deleted_user_id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $dates = [
        'created_at','updated_at','deleted_at'
    ];

    protected $appends = [
        'url_delete_data',
        'url_edit_data',
        'url_image',
    ];

    public function getUrlDeleteDataAttribute()
    {
        return route('api.backend.manuRestaurant.delete',$this->attributes['id']);
    }

    public function getUrlEditDataAttribute()
    {
        return route('api.backend.manuRestaurant.edit',$this->attributes['id']);
    }

    public function getFullPath()
    {
        return Config::get('storage.menuRestaurant') . '/' . e($this->attributes['image']);
    }

    public function getUrlImageAttribute()
    {
        return $this->attributes['image'] ? route('api.backend.manuRestaurant.showImage',$this->attributes['image']) : route('api.backend.manuRestaurant.showImage',-1);
    }

    static function random($image){
        $ret = [];
        $path = Config::get('storage.menuRestaurant');
        $extension = $image->getClientOriginalExtension();
        $try = 250;

        do {
            if ($try <= 0)
                throw Exception("Failed to produce randomized filename");

            $hash = static::cleanFilename(Str::random(32));
            if ($extension)
                $hash = $hash . '.' . $extension;
            $file = $path . '/' . $hash;
            $try -= 1;
        } while (file_exists($file));

        $ret = [$path, $hash];
        return $ret;
    }

    static function cleanFilename($value){
        $pattern = '/[^a-zA-Z0-9\-_\.& \(\)]+/';
        $replacement = '-';
        $value = preg_replace($pattern, $replacement, $value);
        return $value;
    }
}
