<?php namespace App\Models;

use Config;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens,HasFactory, Notifiable;

    protected $fillable = [
        'name',
        'employee_number',
        'nationality_number',
        'home_address',
        'date_of_birth',
        'phone_number',
        'photo',
        'is_super_admin',
        'email',
        'password',
        'joining_date',
        'created_at',
        'updated_at',
        'deleted_at',
        'email_verified_at',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $dates = [
        'email_verified_at','created_at','updated_at','deleted_at','joining_date'
    ];

    protected $appends = [
        'url_delete_data',
        'url_edit_data',
        'url_photo',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getUrlDeleteDataAttribute()
    {
        return route('api.backend.user.delete',$this->attributes['id']);
    }

    public function getUrlEditDataAttribute()
    {
        return route('api.backend.user.edit',$this->attributes['id']);
    }

    public function getUrlPhotoAttribute()
    {
        return $this->attributes['photo'] ? route('api.backend.user.showImage',$this->attributes['photo']) : route('api.backend.user.showImage',-1);
    }

    public function getFullPath()
    {
        return Config::get('storage.user') . '/' . e($this->attributes['photo']);
    }

    public function getNameAttribute($value)
	{
	    return ucwords($value);
    }

    public function getEmailVerifiedAtAttribute($value)
	{
        return $value ?  date('Y-m-d h:i:s', strtotime($value)) :  null;
    }

    public function getCreatedAtAttribute($value)
	{
        return $value ?  date('Y-m-d h:i:s', strtotime($value)) :  null;
    }

    public function getUpdatedAtAttribute($value)
	{
        return $value ?  date('Y-m-d h:i:s', strtotime($value)) :  null;
    }

    public function getJoiningDateAttribute($value)
	{
        return $value ?  date('d/m/Y', strtotime($value)) :  null;
    }

    static function generateEmployeeNumber($joining_date)
    {
        $total = User::whereRaw("DATE_FORMAT(joining_date, '%y%m') = '$joining_date'")->count();

        if($total == 0) return '001';
        if($total > 0 && $total < 10) return '00'.$total;
        if($total > 10 && $total < 100) return '0'.$total;
        else return $total;
    }

    static function random($image){
        $ret = [];
        $path = Config::get('storage.user');
        $extension = $image->getClientOriginalExtension();
        $try = 250;

        do {
            if ($try <= 0)
                throw Exception("Failed to produce randomized filename");

            $hash = static::cleanFilename(Str::random(32));
            if ($extension)
                $hash = $hash . '.' . $extension;
            $file = $path . '/' . $hash;
            $try -= 1;
        } while (file_exists($file));

        $ret = [$path, $hash];
        return $ret;
    }

    static function cleanFilename($value){
        $pattern = '/[^a-zA-Z0-9\-_\.& \(\)]+/';
        $replacement = '-';
        $value = preg_replace($pattern, $replacement, $value);
        return $value;
    }
}
