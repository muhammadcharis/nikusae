<?php namespace App\Http\Controllers\Backend;

use Hash;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;

use App\Models\User;

class HomeController extends Controller
{
    public function login(Request $request)
    {
        try {
            $validator = $request->validate([
                'nik'           => 'required',
                'password'      => 'required'
            ]);

            $user = User::whereNull('deleted_at')
                ->whereNotNull('email_verified_at')
                ->where([
                ['employee_number',$request->nik],
            ])
            ->first();

            if ($user) 
            {
                if (Hash::check($request->password, $user->password)) 
                {
                    return ResponseFormatter::success([
                        'token'      => $user->createToken('web based',['admin'])->plainTextToken,
                    ],'Success');
                        
                } else 
                {
                    return ResponseFormatter::error([
                        'password'  => ['password doesn\'t match']
                    ],'Authentication failed',500);
                }
            }else
            {
                return ResponseFormatter::error([
                    'nik'  => ['User is no exists']
                ],'Authentication failed',500);
            
            }

        } catch (Exception $error) {
            return ResponseFormatter::error([
                'nik'  => ['User is no exists']
            ],'Authentication failed',500);
        } 
    }

    public function logout(Request $request)
    {
        $user = request()->user();
        $user->tokens()->where('id', $user->currentAccessToken()->id)->delete();
        return ResponseFormatter::success([],'Success');
    }
}
