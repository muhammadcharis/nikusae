<?php namespace App\Http\Controllers\Backend;

use File;
use Config;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;

use App\Models\MenuRestaurant;

class MenuRestaurantController extends Controller
{
    public function data (Request $request){
        $keyword =  strtolower(trim($request->searchTerm));
        $last_id =  $request->lastId;

        $data = MenuRestaurant::whereNull('deleted_at');

        if($keyword)
        {
            $data = $data->whereRaw("
                (lower(name) LIKE '%$keyword%'
                or lower(description) LIKE '%$keyword%'
            ");

            if($last_id == -2) $data = $data->where('id',$last_id);

            $last_id = '-2';

            $data = $data
            ->orderBy('id','desc')
            ->get();
        }else
        {
            if($last_id && $last_id != -1) $data = $data->where('id','<',$last_id);
            
            $data = $data->limit(20)
            ->orderBy('id','desc')
            ->get();

            $last_id = '';
            foreach ($data as $key => $datum) 
            {
                $last_id = $datum->id;
            }

        } 

        return ResponseFormatter::success([
            'menuRestaurants'   => $data,
            'lastID'            => $last_id,
        ],'Success');
    }

    public function store (Request $request){
        $storage = Config::get('storage.menuRestaurant');
        if (!File::exists($storage)) File::makeDirectory($storage, 0777, true);

        $created_at             = carbon::now()->todatetimestring();
        $is_image_changed       =  $request->isImageChanged;
        $_id                    =  $request->_id;
        $name                   =  trim($request->name);
        $description            =  trim($request->description);
        $image                  =  $request->file('image');
        $price                  =  trim($request->price);

        if($_id == -1)
        {
            if($image) $image_name  = MenuRestaurant::random($image)[1];
            else $image_name = null;

            $menu_restaurant = MenuRestaurant::Create([
                'name'                  => $name,
                'description'           => $description,
                'price'                 => $price,
                'image'                 => $image_name,
                'created_at'            => $created_at,
                'updated_at'            => $created_at,
                'created_user_id'       => $request->user()->id,
                'updated_user_id'       => $request->user()->id,
            ]);

            if($menu_restaurant->save() && $image)
            {
                $image->move($storage, $image_name);
            }

        }else
        {
            $menu_restaurant = MenuRestaurant::find($_id);
            if($menu_restaurant)
            {
                $old_image = $menu_restaurant->getFullPath();
                
                if($is_image_changed)
                {
                    if($image) $image_name  = MenuRestaurant::random($image)[1];
                    else $image_name = null;
                }else $image_name = null;
                
                if($image_name) $menu_restaurant->image             = $image_name;
                
                $menu_restaurant->name              = $name;
                $menu_restaurant->description       = $description;
                $menu_restaurant->price             = $price;
                $menu_restaurant->updated_at        = $created_at;
                $menu_restaurant->updated_user_id   = $request->user()->id;
                
                if($menu_restaurant->save() && $image_name)
                {
                    if(File::exists($old_image)) @unlink($old_image);
                    $image->move($storage, $image_name);
                }
                
            }
        }
        

        return ResponseFormatter::success('Success');
    }

    public function edit (Request $request,$id){
        $menu_restaurant = MenuRestaurant::find($id);
        return ResponseFormatter::success([
            'menuRestaurant' => $menu_restaurant
        ],'Success');
    }

    public function delete (Request $request,$id){
        $menu_restaurant = MenuRestaurant::find($id);
        
        if($menu_restaurant)
        {
            if(!$menu_restaurant->deleted_at)
            {
                $old_image = $menu_restaurant->getFullPath();
                if(File::exists($old_image)) @unlink($old_image);
                $menu_restaurant->deleted_at = carbon::now()->todatetimestring();
                $menu_restaurant->save();
            }
        }

        return ResponseFormatter::success('Success');
    }

    public function showImage($filename)
    {
        if($filename == -1)
        {
            $resp = response()->download(public_path("images/image_not_found.png"));
            $resp->headers->set('Content-Disposition', 'inline');
            $resp->headers->set('X-Content-Type-Options', 'nosniff');
            return $resp;
        }else
        {
            $path = Config::get('storage.menuRestaurant');
            $resp = response()->download($path.'/'.$filename);
            $resp->headers->set('Content-Disposition', 'inline');
            $resp->headers->set('X-Content-Type-Options', 'nosniff');
            return $resp;
        }
       
    }

}
