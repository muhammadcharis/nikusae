<?php namespace App\Http\Controllers\Backend;

use File;
use Config;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

use App\Models\User;

class UserController extends Controller
{
    public function data (Request $request){
        $keyword =  strtolower(trim($request->searchTerm));
        $last_id =  $request->lastId;

        $data = User::whereNull('deleted_at');

        if($keyword)
        {
            $data = $data->whereRaw("
                (lower(name) LIKE '%$keyword%'
                or lower(phone_number) LIKE '%$keyword%'
                or lower(nationality_number) LIKE '%$keyword%'
                or lower(employee_number) LIKE '%$keyword%')
            ");

            if($last_id == -2) $data = $data->where('id',$last_id);

            $last_id = '-2';

            $data = $data
            ->orderBy('id','desc')
            ->get();
        }else
        {
            if($last_id && $last_id != -1) $data = $data->where('id','<',$last_id);

            $data = $data->limit(20)
            ->orderBy('id','desc')
            ->get();
            
            $last_id = '';
            foreach ($data as $key => $datum) 
            {
                $last_id = $datum->id;
            }
        } 
        
        return ResponseFormatter::success([
            'users'      => $data,
            'lastID'     => $last_id,
        ],'Success');
    }

    public function store (Request $request){
        $storage = Config::get('storage.user');
        if (!File::exists($storage)) File::makeDirectory($storage, 0777, true);

        $created_at             = carbon::now()->todatetimestring();
        $_id                    =  $request->_id;
        $name                   =  trim($request->name);
        $nationality_number     =  trim($request->nationality_number);
        $home_address           =  trim($request->home_address);
        $date_of_birth          =  trim($request->date_of_birth);
        $joining_date           =  trim($request->joining_date);
        $phone_number           =  trim($request->phone_number);
        $email                  =  trim($request->email);
        $is_image_changed       =  $request->isImageChanged;
        $image                  =  $request->file('photo');

        if($_id == -1)
        {
            $is_nationality_exists  = User::where('nationality_number',$nationality_number)->exists();
            if($is_nationality_exists) return ResponseFormatter::error(['msg' => 'ID Card already exists'],'Insert failed',422);
            
            $is_phone_number_exists = User::where('phone_number',$phone_number)->exists();
            if($is_phone_number_exists) return ResponseFormatter::error(['msg' => 'Phone number already exists'],'Insert failed',422);
            
            $is_email_exists        = User::where('email',$email)->exists();
            if($is_email_exists) return ResponseFormatter::error(['msg' => 'Email already exists'],'Insert failed',422);
            
            if($image) $image_name  = User::random($image)[1];
            else $image_name = null;

            $user = User::Create([
                'nationality_number'    => $nationality_number,
                'name'                  => $name,
                'photo'                 => $image_name,
                'date_of_birth'         => $date_of_birth,
                'joining_date'          => $joining_date,
                'phone_number'          => $phone_number,
                'email'                 => $email,
                'home_address'          => $home_address,
                'is_super_admin'        => false,
                'password'              => Hash::make($date_of_birth),
                'created_at'            => $created_at,
                'updated_at'            => $created_at,
            ]);
    
            if($user->save())
            {
                $user->employee_number = $user->joining_date->format('ym').$user->generateEmployeeNumber($user->joining_date->format('ym'));
                $user->save();

                if($image)
                {
                    $image->move($storage, $image_name);
                }
            }
        }else
        {
            $user = User::find($_id);
            if($user)
            {
                $is_nationality_exists  = User::where([
                    ['nationality_number',$nationality_number],
                    ['id','!=',$user->id],
                ])
                ->exists();
                if($is_nationality_exists) return ResponseFormatter::error(['msg' => 'ID Card already exists'],'Insert failed',422);
                
                $is_phone_number_exists = User::where([
                    ['phone_number',$phone_number],
                    ['id','!=',$user->id],
                ])
                ->exists();
                if($is_phone_number_exists) return ResponseFormatter::error(['msg' => 'Phone number already exists'],'Insert failed',422);
                
                $is_email_exists        = User::where([
                    ['email',$email],
                    ['id','!=',$user->id],
                ])
                ->exists();
                if($is_email_exists) return ResponseFormatter::error(['msg' => 'Email already exists'],'Insert failed',422);

                if($is_image_changed)
                {
                    if($image) $image_name  = User::random($image)[1];
                    else $image_name = null;
                }else $image_name = null;

                if($image_name) $user->photo             = $image_name;

                $old_image = $user->getFullPath();

                $user->nationality_number   = $nationality_number;
                $user->name                 = $name;
                $user->date_of_birth        = $date_of_birth;
                $user->phone_number         = $phone_number;
                $user->email                = $email;
                $user->home_address         = $home_address;
                $user->updated_at           = $created_at;

                if($user->save() && $image_name)
                {
                    if(File::exists($old_image)) @unlink($old_image);
                    $image->move($storage, $image_name);
                }
            }
        }
        

        return ResponseFormatter::success('Success');
    }

    public function edit (Request $request,$id){
        $user = User::find($id);
        return ResponseFormatter::success([
            'user' => $user
        ],'Success');
    }

    public function delete (Request $request,$id){
        $user = User::find($id);
        
        if($user)
        {
            if(!$user->deleted_at)
            {
                $old_image = $user->getFullPath();
                if(File::exists($old_image)) @unlink($old_image);

                $user->deleted_at = carbon::now()->todatetimestring();
                $user->save();
            }
           
        }

        return ResponseFormatter::success('Success');
    }

    public function showImage($filename)
    {
        if($filename == -1)
        {
            $resp = response()->download(public_path("images/male_avatar.png"));
            $resp->headers->set('Content-Disposition', 'inline');
            $resp->headers->set('X-Content-Type-Options', 'nosniff');
            return $resp;
        }else
        {
            $path = Config::get('storage.user');
            $resp = response()->download($path.'/'.$filename);
            $resp->headers->set('Content-Disposition', 'inline');
            $resp->headers->set('X-Content-Type-Options', 'nosniff');
            return $resp;
        }
       
    }
}
