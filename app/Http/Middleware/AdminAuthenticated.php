<?php namespace App\Http\Middleware;

use Closure;
use App\Helpers\ResponseFormatter;

class AdminAuthenticated
{
   
    public function handle($request, Closure $next)
    {
        if (auth()->user()->tokenCan('admin')) {
            return $next($request);
        }

        return ResponseFormatter::error('Not Authorized',401);
    }
}
