const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .combine([
        'resources/js/admin/vendors.min.js',
        'resources/js/admin/LivIconsEvo/js/LivIconsEvo.tools.js',
        'resources/js/admin/LivIconsEvo/js/LivIconsEvo.defaults.js',
        'resources/js/admin/LivIconsEvo/js/LivIconsEvo.min.js',
        'resources/js/admin/vertical-menu-light.js',
        'resources/js/admin/app-menu.js',
        'resources/js/admin/app.js',
        'resources/js/admin/footer.js',
        'resources/js/admin/sweetalert2.all.min.js',
    ], 'public/js/templete_admin.js')
    .combine([
        'resources/js/admin/charts/apexcharts.min.js',
    ], 'public/js/apexcharts.js')
    .combine([
        'resources/css/admin/vendors.min.css',
        'resources/css/admin/swiper.min.css',
        'resources/css/admin/colors.css',
        'resources/css/admin/components.css',
        //'resources/css/admin/dark-layout.css',
        'resources/css/admin/semi-dark-layout.css',
        'resources/css/admin/vertical-menu.css',
        'resources/css/admin/sweetalert2.min.css',
        'resources/css/admin/style.css',
    ], 'public/css/templete_admin.css')
    .combine([
        'resources/css/admin/charts/apexcharts.css',
        'resources/css/admin/charts/chartist.min.css',
    ], 'public/css/charts.css')
    .combine([
        'resources/css/bootstrap.min.css',
        'resources/css/bootstrap-extended.css',
    ], 'public/css/bootstrap.css')
    .vue()
    .copyDirectory('resources/fonts', 'public/fonts')
    .copyDirectory('resources/images', 'public/images')
    .sass('resources/sass/app.scss', 'public/css');