<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <meta name="description" content="Nikusae ADMIN.">
        <meta name="keywords" content="Nikusae">
        <meta name="author" content="CHARIS 2021">

        <meta property="og:url" content="http://nikusae-blora.com/">
        <meta property="og:image" content="http://nikusae-blora.com/logo">
        <meta property="og:image:type" content="image/png">
    	<meta property="og:image:width" content="650">
    	<meta property="og:image:height" content="366">
        <meta property="og:description" content="Nikusae">
        <meta property="og:title" content="Nikusae">
        <meta property="og:type" content="website">
        <meta property="og:locale" content="en_ID">

        <title>Nikusae</title>
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        
        <link rel="bif-icon" href="{{ asset('images/favicon.ico') }}">
        <link type="image/x-icon" rel="shortcut icon" href="http://nikusae-blora.com/logo">
        <link rel="shortcut icon" type="image/x-icon" href="{{ asset('images/favicon.ico') }}">
    
        <script src="{{ asset('js/app.js') }}" defer></script>
        @yield('styles')
        
    </head>

    <body>
        <div id="app">
        </div>

        @yield('scripts')
       </body>
</html>