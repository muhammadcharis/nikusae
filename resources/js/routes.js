import Login from './components/backend/Login';
import NotFound from './components/backend/404';
import Dashboard from './components/backend/Dashboard';
import Cashier from './components/backend/Cashier';
import User from './components/backend/User';
import MenuRestaurant from './components/backend/MenuRestaurant';
import store from './store';

export default {
    mode: 'history',
    linkActiveClass: 'active',
    inkExactActiveClass: 'active',
    routes: [{
            path: '*',
            component: NotFound
        },
        {
            path: '/backend',
            beforeEnter: (to, form, next) => {
                const headers = {
                    'Content-Type': 'application/json',
                    "Authorization": "Bearer " + store.state.token,
                };

                axios.get('/api/admin-authenticated', { headers: headers }).then(() => {
                    store.commit('adminAuthenticate', {
                        isAdminLogin: true,
                        user: response.data
                    });

                    next({ name: 'dashboard_backend' });
                }).catch(() => {
                    next({ name: 'login' });
                });
            }
        },
        {
            path: '/backend/login',
            component: Login,
            name: 'login',
            beforeEnter: (to, form, next) => {
                const headers = {
                    'Content-Type': 'application/json',
                    "Authorization": "Bearer " + store.state.token,
                };

                axios.get('/api/admin-authenticated', { headers: headers }).then((response) => {
                    store.commit('adminAuthenticate', {
                        isAdminLogin: true,
                        token: store.state.token,
                        user: response.data
                    });

                    next({ name: 'dashboard_backend' });
                }).catch(() => {
                    store.commit('adminAuthenticate', {
                        isAdminLogin: false,
                        token: '',
                        user: {}
                    });

                    next();
                    console.clear();
                });
            }
        },
        {
            path: "/backend/dashboard",
            name: "dashboard_backend",
            component: Dashboard,
            beforeEnter: (to, form, next) => {
                const headers = {
                    'Content-Type': 'application/json',
                    "Authorization": "Bearer " + store.state.token,
                };

                axios.get('/api/admin-authenticated', { headers: headers }).then((response) => {
                    store.commit('adminAuthenticate', {
                        isAdminLogin: true,
                        token: store.state.token,
                        user: response.data
                    });


                    next();
                }).catch(() => {
                    store.commit('userAuthenticated', {
                        isAdminLogin: false,
                        token: '',
                        user: {}
                    });


                    next({ name: 'login' });
                    console.clear();
                });
            }

        },
        {
            path: "/backend/user",
            name: "user",
            component: User,
            beforeEnter: (to, form, next) => {
                const headers = {
                    'Content-Type': 'application/json',
                    "Authorization": "Bearer " + store.state.token,
                };

                axios.get('/api/admin-authenticated', { headers: headers }).then((response) => {
                    store.commit('adminAuthenticate', {
                        isAdminLogin: true,
                        token: store.state.token,
                        user: response.data
                    });


                    next();
                }).catch(() => {
                    store.commit('userAuthenticated', {
                        isAdminLogin: false,
                        token: '',
                        user: {}
                    });


                    next({ name: 'login' });
                    console.clear();
                });
            }

        },
        {
            path: "/backend/menu-restaurant",
            name: "MenuRestaurant",
            component: MenuRestaurant,
            beforeEnter: (to, form, next) => {
                const headers = {
                    'Content-Type': 'application/json',
                    "Authorization": "Bearer " + store.state.token,
                };

                axios.get('/api/admin-authenticated', { headers: headers }).then((response) => {
                    store.commit('adminAuthenticate', {
                        isAdminLogin: true,
                        token: store.state.token,
                        user: response.data
                    });


                    next();
                }).catch(() => {
                    store.commit('userAuthenticated', {
                        isAdminLogin: false,
                        token: '',
                        user: {}
                    });


                    next({ name: 'login' });
                    console.clear();
                });
            }

        }, {
            path: "/backend/cashier",
            name: "Cashier",
            component: Cashier,
            beforeEnter: (to, form, next) => {
                const headers = {
                    'Content-Type': 'application/json',
                    "Authorization": "Bearer " + store.state.token,
                };

                axios.get('/api/admin-authenticated', { headers: headers }).then((response) => {
                    store.commit('adminAuthenticate', {
                        isAdminLogin: true,
                        token: store.state.token,
                        user: response.data
                    });

                    next();
                }).catch(() => {
                    store.commit('userAuthenticated', {
                        isAdminLogin: false,
                        token: '',
                        user: {}
                    });

                    next({ name: 'login' });
                    console.clear();
                });
            }

        }

    ]
}