import Vuex from 'vuex';
import Vue from 'vue'

Vue.use(Vuex);
const store = new Vuex.Store({
    state: {
        token: window.localStorage.getItem('token'),
        selectedCrumb: '',
        crumbs: [],
        isAdminLogin: false,
        user: {},
    },
    mutations: {
        adminAuthenticate(state, payload) {
            state.isAdminLogin = payload.isAdminLogin;
            state.token = payload.token;
            state.user = payload.user;

            localStorage.setItem('token', payload.token);
        },
        crumb(state, payload) {
            state.selectedCrumb = payload.selectedCrumb;
            state.crumbs = payload.crumbs;
        },
    }
})

export default store;