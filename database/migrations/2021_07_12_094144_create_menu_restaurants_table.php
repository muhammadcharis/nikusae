<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenuRestaurantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_restaurants', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('menu_category_id',36);
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('image')->nullable();
            $table->string('price')->nullable()->default(0);
            $table->bigInteger('created_user_id')->unsigned();
            $table->bigInteger('updated_user_id')->unsigned();
            $table->bigInteger('deleted_user_id')->unsigned()->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();

            $table->foreign('menu_category_id')->references('id')->on('menu_categories')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('created_user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('updated_user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('deleted_user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_restaurants');
    }
}
