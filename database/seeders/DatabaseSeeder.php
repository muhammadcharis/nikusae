<?php namespace Database\Seeders;

use DB;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;

use Faker\Factory as Faker;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        //DB::table('menu_restaurants')->truncate();
        DB::table('users')->delete();
        DB::table('users')->insert([
            'employee_number'   => '001',
            'phone_number'      => '081296412724',
            'name'              => 'muhammad charis azwar',
            'email'             => 'muhammad.charis.azwar@gmail.com',
            'password'          => Hash::make('password1'),
            'is_super_admin'    => true,
            'email_verified_at' => carbon::now()->todatetimestring(),
            'created_at'        => carbon::now()->todatetimestring(),
            'updated_at'        => carbon::now()->todatetimestring(),
        ]);

        /*$faker = Faker::create('id_ID');
        for($i = 1; $i <= 150; $i++){
            // insert data ke table siswa menggunakan Faker
            $date_of_birth = $faker->dateTime()->format('Y-m-d');
            \DB::table('users')->insert([
                'employee_number' => $faker->randomDigit,
                'name' => $faker->name,
                'email' => $faker->email,
                'is_super_admin' => false,
                'date_of_birth' => $date_of_birth,
                'password'          => Hash::make($date_of_birth),
                'home_address' => $faker->address,
                'phone_number' => $faker->phoneNumber
            ]);
        }*/

    }
}
