<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('/admin-authenticated', function()  // ini buat cek admin
{
    return auth()->user();
})
->middleware(['auth:sanctum','admin.auth']);

Route::group(['prefix' => 'backend', 'namespace' => 'Backend'], function ()
{
    Route::post('/login', [App\Http\Controllers\Backend\HomeController::class, 'login'])->name('api.backend.login');
    Route::post('/logout', [App\Http\Controllers\Backend\HomeController::class, 'logout'])->name('api.backend.logout')->middleware(['auth:sanctum','admin.auth']);

    Route::group(['prefix' => 'user'], function ()
    {
        Route::get('/data', [App\Http\Controllers\Backend\UserController::class, 'data'])->name('api.backend.user.data')->middleware(['auth:sanctum','admin.auth']);  
        Route::get('/edit/{id}', [App\Http\Controllers\Backend\UserController::class, 'edit'])->name('api.backend.user.edit')->middleware(['auth:sanctum','admin.auth']);  
        Route::get('/show-image/{id}', [App\Http\Controllers\Backend\UserController::class, 'showImage'])->name('api.backend.user.showImage');
        Route::post('/store', [App\Http\Controllers\Backend\UserController::class, 'store'])->name('api.backend.user.store')->middleware(['auth:sanctum','admin.auth']);  
        Route::post('/delete/{id}', [App\Http\Controllers\Backend\UserController::class, 'delete'])->name('api.backend.user.delete')->middleware(['auth:sanctum','admin.auth']);  
        Route::post('/update/{id}', [App\Http\Controllers\Backend\UserController::class, 'update'])->name('api.backend.user.update')->middleware(['auth:sanctum','admin.auth']);  
    });

    Route::group(['prefix' => 'menu-restaurant'], function ()
    {
        Route::get('/data', [App\Http\Controllers\Backend\MenuRestaurantController::class, 'data'])->name('api.backend.manuRestaurant.data');//->middleware(['auth:sanctum','admin.auth']);  
        Route::get('/edit/{id}', [App\Http\Controllers\Backend\MenuRestaurantController::class, 'edit'])->name('api.backend.manuRestaurant.edit')->middleware(['auth:sanctum','admin.auth']);  
        Route::get('/show-image/{id}', [App\Http\Controllers\Backend\MenuRestaurantController::class, 'showImage'])->name('api.backend.manuRestaurant.showImage'); 
        Route::post('/store', [App\Http\Controllers\Backend\MenuRestaurantController::class, 'store'])->name('api.backend.manuRestaurant.store')->middleware(['auth:sanctum','admin.auth']);  
        Route::post('/delete/{id}', [App\Http\Controllers\Backend\MenuRestaurantController::class, 'delete'])->name('api.backend.manuRestaurant.delete')->middleware(['auth:sanctum','admin.auth']);  
        Route::post('/update/{id}', [App\Http\Controllers\Backend\MenuRestaurantController::class, 'update'])->name('api.backend.manuRestaurant.update')->middleware(['auth:sanctum','admin.auth']);  
    });
});